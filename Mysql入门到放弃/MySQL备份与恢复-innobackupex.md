# [MySQL备份与恢复-innobackupex](https://www.cnblogs.com/wxzhe/p/10033983.html)



**索引目录**

- 一个完全备份
  - [ 增量备份与恢复](https://www.cnblogs.com/wxzhe/p/10033983.html#_label0_0)
  - [流压缩](https://www.cnblogs.com/wxzhe/p/10033983.html#_label0_1)

 

------

　：上一片myloder搞崩溃，为什么百度的博文都是抄袭一模一样的，哎烦！

这一片文章我们来介绍物理备份工具xtracebackup!

首先是安装可以percona官网下载安装，下载rpm包直接yum安装即可！

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
yum install -y perl-DBD-MySQL perl-DBI perl-Time-HiRes libaio*         #安装依赖包
yum install -y percona-xtrabackup-24-2.4.4-1.el6.x86_64.rpm
[root@test2 ~]# xtrabackup -v
xtrabackup version 2.4.4 based on MySQL server 5.7.13 Linux (x86_64) (revision id: df58cf2)
#下载的时候尽量下载新版本的percoan，这样会有对MySQL5.7的支持
#写这个博文的时候，官网最新版本好像是8.0的吧，这个8.0版本应该是为了支持MySQL8.x吧，后续再测一下！
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

XtraceBackup是由知名数据库软件服务企业Percona提供的一款热备工具，除了能够支持最为常见的MyISAM,INNODB引擎对象外，还支持XtraDB引擎。

查看percona的安装文件：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
[root@test2 ~]# rpm -ql percona-xtrabackup-24-2.4.4-1.el6.x86_64
/usr/bin/innobackupex 
/usr/bin/xbcloud
/usr/bin/xbcloud_osenv
/usr/bin/xbcrypt
/usr/bin/xbstream                 #以专用的xbstream格式压缩xtrabackup输出的信息。也可以使用tar 
/usr/bin/xtrabackup               #最经常使用的就是这两个备份工具，其中之前的版本xtracebackup只能备份INNODB存储引擎，而innobackupex对xtracebackup进行了封装，不仅能备份INNODB存储引擎，也能备份myisam存储引擎，但是查看官方2.4版本的文档时候，percona说在之后的版本会放弃innobackupex这个工具，建议使用xtracebackup命令！
/usr/share/doc/percona-xtrabackup-24-2.4.4
/usr/share/doc/percona-xtrabackup-24-2.4.4/COPYING
/usr/share/man/man1/innobackupex.1.gz
/usr/share/man/man1/xbcrypt.1.gz
/usr/share/man/man1/xbstream.1.gz
/usr/share/man/man1/xtrabackup.1.gz
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

 

XtraceBackup备份有以下几个优点：

l 备份集高效，完整，可用。

l 备份任务执行过程中不会有阻塞任务

l 节省磁盘空间，降低网络带宽占用

l 备份集自动验证机制

l 恢复更快

xtrabackup必须在MySQL服务端执行（但是创建的备份集不一定是保存在本地），特别是通过innobackupex命令创建备份集时，由于操作需要连接数据库获取信息，因此还要指定的连接参数（用户名，密码），而且连接所使用的的用户，必须拥有正确的操作权限。

特别提醒：

【

percona官方在文档中说要在之后的版本中放弃innobackupex工具，推荐使用xtracebackupex工具，但是截至目前为止，我们公司线上环境的备份还是使用的innobackupex。因此这里我们首先介绍innobackupex的备份使用，然后再介绍xtracebackup的使用。

xtraceback2.4.x的官方文档地址：[CLICK HEERE](https://www.percona.com/doc/percona-xtrabackup/2.4/index.html)

】

### innobackupex的备份与恢复

innobackupex的参数有很多，仅通过实例来讲解经常使用的，若需要某些参数可以查看官方文档！

[回到顶部](https://www.cnblogs.com/wxzhe/p/10033983.html#_labelTop)

## 一个完全备份

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
[root@test3 ~]# innobackupex --user=root --password=123456  /data/backup/          #备份很简单，/data/backup/为备份的目录
181128 18:50:47 innobackupex: Starting the backup operation

IMPORTANT: Please check that the backup run completes successfully.
           At the end of a successful backup run innobackupex
           prints "completed OK!".

181128 18:50:47  version_check Connecting to MySQL server with DSN 'dbi:mysql:;mysql_read_default_group=xtrabackup;host=localhost' as 'root'  (using password: YES).
181128 18:50:47  version_check Connected to MySQL server
181128 18:50:47  version_check Executing a version check against the server...
181128 18:50:47  version_check Done.181128 18:50:47 Connecting to MySQL server host: localhost, user: root, password: set, port: 0, socket: (null)Using server version 5.7.22-loginnobackupex version 2.4.4 based on MySQL server 5.7.13 Linux (x86_64) (revision id: df58cf2)xtrabackup: uses posix_fadvise().xtrabackup: cd to /data/mysqlxtrabackup: open files limit requested 0, set to 1024xtrabackup: using the following InnoDB configuration:xtrabackup:   innodb_data_home_dir = .xtrabackup:   innodb_data_file_path = ibdata1:12M:autoextendxtrabackup:   innodb_log_group_home_dir = ./xtrabackup:   innodb_log_files_in_group = 2xtrabackup:   innodb_log_file_size = 50331648InnoDB: Number of pools: 1181128 18:50:47 >> log scanned up to (13741543758)xtrabackup: Generating a list of tablespacesInnoDB: Allocated tablespace ID 48 for mysql/servers, old maximum was 0181128 18:50:48 [01] Copying ./ibdata1 to /data/backup/2018-11-28_18-50-47/ibdata1   #拷贝的是ibd文件，然后分别拷贝每张表的文件，然后再拷贝redo日志181128 18:50:48 >> log scanned up to (13741543758)181128 18:50:49 >> log scanned up to (13741543758)181128 18:50:50 [01]        ...done181128 18:50:50 [01] Copying ./mysql/servers.ibd to /data/backup/2018-11-28_18-50-47/mysql/servers.ibd181128 18:50:50 [01]        ...done181128 18:50:50 [01] Copying ./mysql/time_zone_name.ibd to /data/backup/2018-11-28_18-50-47/mysql/time_zone_name.ibd
......
MySQL binlog position: filename 'test3-bin.000001', position '21818'
181128 18:51:12 [00] Writing backup-my.cnf
181128 18:51:12 [00]        ...done
181128 18:51:12 [00] Writing xtrabackup_info
181128 18:51:12 [00]        ...done
xtrabackup: Transaction log of lsn (13741543749) to (13741543758) was copied.
181128 18:51:12 completed OK!         #出现completed OK表示备份完成！因为过程比较长，没有粘贴，可以看一下备份的过程！
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

innobackupex会在备份目录下面创建以当前时间点命名的备份文件如下：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
[root@test3 ~]# cd /data/backup/
[root@test3 backup]# ls
2018-11-28_18-50-47             #以当前时间点命名的备份文件集
[root@test3 backup]# cd 2018-11-28_18-50-47/
[root@test3 2018-11-28_18-50-47]# ls          #看到的备份之后的文件集
backup-my.cnf  employees   hostinfo        ibdata1  mysql   performance_schema  sys   tpcc_test  xtrabackup_binlog_info  xtrabackup_info
cmdb           financesys  ib_buffer_pool  lianxi   mytest  sbtest              test  ts1.ibd    xtrabackup_checkpoints  xtrabackup_logfile[root@test3 2018-11-28_18-50-47]# cd employees; ls     #每个数据库中包含的表的结构文件（frm）和表数据文件（ibd）current_dept_emp.frm  departments.frm  dept_emp.frm  dept_emp_latest_date.frm  dept_manager.ibd  employees.ibd  salaries.ibd  test2.frm  titles.frmdb.opt                departments.ibd  dept_emp.ibd  dept_manager.frm          employees.frm     salaries.frm   test1.frm     test3.frm  titles.ibd[root@test3 employees]#
backup-my.cnf：　为当前数据库配置文件的备份！
xtrabackup_binlog_info：当前备份文件对应的二进制日志文件和position位置，做PIT恢复时使用。

[root@test3 2018-11-28_18-50-47]# cat xtrabackup_info      #包含信息比较多
uuid = 85662dd9-f2fb-11e8-a47c-fa336351fc00                #当前数据库的UUID
name = 
tool_name = innobackupex                                   #包含备份的工具，备份的命令，备份工具的版本，MySQL的版本
tool_command = --user=root --password=... /data/backup/
tool_version = 2.4.4
ibbackup_version = 2.4.4
server_version = 5.7.22-log
start_time = 2018-11-28 18:50:47                           #备份的开始时间
end_time = 2018-11-28 18:51:12                             #备份的结束时间
lock_time = 0
binlog_pos = filename 'test3-bin.000001', position '21818'       #二进制日志的位置
innodb_from_lsn = 0
innodb_to_lsn = 13741543749                                      #刷新到lsn的位置
partial = N
incremental = N                                                  #增量备份
format = file
compact = N                                        
compressed = N
encrypted = N

[root@test3 2018-11-28_18-50-47]# cat xtrabackup_checkpoints        #包含checkpoint的信息，以及备份方式
backup_type = full-backuped
from_lsn = 0
to_lsn = 13741543749
last_lsn = 13741543758
compact = 0
recover_binlog_info = 0
xtrabackup_logfile：【未知】
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

执行恢复

恢复分为两个步骤：

准备恢复：所谓准备恢复，就是要为恢复做准备。就是说备份集没办法直接拿来用，因为这中间可能存在未提交或未回滚的事务，数据文件不一致，所以需要一个队备份集的准备过程。

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
[root@test3 ~]# innobackupex --apply-log /data/backup/2018-11-28_18-50-47/　　　　　　　　＃准备阶段的命令　　　
181128 19:16:57 innobackupex: Starting the apply-log operation

IMPORTANT: Please check that the apply-log run completes successfully.
           At the end of a successful apply-log run innobackupex
           prints "completed OK!".

innobackupex version 2.4.4 based on MySQL server 5.7.13 Linux (x86_64) (revision id: df58cf2)
xtrabackup: cd to /data/backup/2018-11-28_18-50-47
xtrabackup: This target seems to be not prepared yet.
InnoDB: Number of pools: 1
．．．．．
InnoDB: 5.7.13 started; log sequence number 13741544981
xtrabackup: starting shutdown with innodb_fast_shutdown = 1
InnoDB: FTS optimize thread exiting.
InnoDB: Starting shutdown...
InnoDB: Shutdown completed; log sequence number 13741545378
181128 19:17:02 completed OK!
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

执行恢复：innobackupex提供了--copy-back参数，就是将指定的备份集，恢复到指定的路径下面（这个指定的路径是配置文件中datadir指定的路径）！

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
[root@test3 ~]# service mysqld stop       #首先停掉数据库
Shutting down MySQL.... SUCCESS! 
[root@test3 ~]# rm -fr /data/mysql/*      #情况datadir指定的目录，若是线上环境，确保一定备份过
[root@test3 ~]# innobackupex --copy-back /data/backup/2018-11-28_18-50-47/     #备份命令
181128 19:20:58 innobackupex: Starting the copy-back operation

IMPORTANT: Please check that the copy-back run completes successfully.
           At the end of a successful copy-back run innobackupex
           prints "completed OK!".

innobackupex version 2.4.4 based on MySQL server 5.7.13 Linux (x86_64) (revision id: df58cf2)
......
181128 19:21:30 [01] Copying ./xtrabackup_info to /data/mysql/xtrabackup_info
181128 19:21:30 [01]        ...done
181128 19:21:30 [01] Copying ./xtrabackup_binlog_pos_innodb to /data/mysql/xtrabackup_binlog_pos_innodb
181128 19:21:30 [01]        ...done
181128 19:21:30 [01] Copying ./xtrabackup_galera_info to /data/mysql/xtrabackup_galera_info
181128 19:21:30 [01]        ...done
181128 19:21:30 [01] Copying ./ibtmp1 to /data/mysql/ibtmp1
181128 19:21:30 [01]        ...done
181128 19:21:30 completed OK!        #恢复完成
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

数据库恢复之后，启动数据库：

```
[root@test3 ~]# chown -R mysql:mysql /data/mysql/
[root@test3 ~]# service mysqld start
Starting MySQL.. SUCCESS!
```

至此一个完全备份的数据恢复工作完成！

在使用innobackupex进行备份时，还可以使用--no-timestamp选项来阻止命令自动创建一个以时间命名的目录，我们可以自定义备份集的名字如下：

```
[root@test3 ~]# innobackupex --user=root --password=123456 --no-timestamp /data/backup/test_`date +%F`  #指定备份集的名字
[root@test3 ~]# cd /data/backup/
[root@test3 backup]# ls           #查看备份集
 test_2018-11-28
[root@test3 backup]#
```

有时候因为socket文件不在默认位置，可以使用--socket指定位置：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
[root@test2 ~]# innobackupex --user=root --password="7abec53701c3eefb" --no-timestamp /data/backup/testdb
181128 19:40:10 innobackupex: Starting the backup operation

IMPORTANT: Please check that the backup run completes successfully.
           At the end of a successful backup run innobackupex
           prints "completed OK!".

181128 19:40:10  version_check Connecting to MySQL server with DSN 'dbi:mysql:;mysql_read_default_group=xtrabackup' as 'root'  (using password: YES).
181128 19:40:10  version_check Connected to MySQL server
181128 19:40:10  version_check Executing a version check against the server...
181128 19:40:10  version_check Done.
181128 19:40:10 Connecting to MySQL server host: localhost, user: root, password: set, port: 0, socket: (null)
Failed to connect to MySQL server: Can't connect to local MySQL server through socket '/tmp/mysql.sock' (2).
[root@test2 ~]# innobackupex --user=root --password="7abec53701c3eefb" --no-timestamp --socket=/var/lib/mysql/mysql.sock  data/backup/testdb
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)



###  增量备份与恢复

增量备份时与全备一起结合使用的，因此这里我们会说明一个全部的过程。

1：首先对目前的数据库做一个全备

```
[root@test3 ~]# innobackupex --user=root --password=123456 --no-timestamp /data/backup/full_backup
[root@test3 ~]# cd /data/backup/
[root@test3 backup]# ls                   #全部备份的文件
full_backup         
```

2：对数据做一些修改，如下

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
mysql> use sbtest;              #创建了一张表，然后插入了两条记录
Database changed
mysql> create table tb3(a varchar(5), sysTime datetime);
Query OK, 0 rows affected (0.02 sec)

mysql> insert into tb3 select "aa", now();
Query OK, 1 row affected (0.01 sec)
Records: 1  Duplicates: 0  Warnings: 0

mysql> insert into tb3 select "bb", now();
Query OK, 1 row affected (0.01 sec)
Records: 1  Duplicates: 0  Warnings: 0

mysql> select * from tb3;
+------+---------------------+
| a    | sysTime             |
+------+---------------------+
| aa   | 2018-11-29 09:14:00 |
| bb   | 2018-11-29 09:14:11 |
+------+---------------------+
2 rows in set (0.00 sec)
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

3：开始增量备份

```
[root@test3 backup]# innobackupex --user=root --password=123456 --no-timestamp --incremental /data/backup/incre1 --incremental-basedir=/data/backup/full_backup
--no-timestamp： 上面已经说过，自己定义备份集的名字
--incremental /data/backup/incre1 ：参数说明这次备份时一个增量备份，后面的路径指定增量备份的目录
--incremental-basedir=/data/backup/full_backup ：增量备份时基于全备的，这个参数就是说明本次的增量备份时基于那次的全备，指向全备的目录文件
```

查看增量备份的文件记录：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
[root@test3 incre1]# cat xtrabackup_checkpoints
backup_type = incremental                 #备份类型
from_lsn = 13741583205                    #需要说明增量备份只能INNODB，xtrack存储引擎，备份是基于lsn的改变的！ 
to_lsn = 13741590096
last_lsn = 13741590105
compact = 0
recover_binlog_info = 0
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

备份完成之后，我们需要恢复操作，记得上面恢复第一步是什么？准备数据，也就是redo已经提交的事务，回滚未提交的事务。增量备份也是这样，但是操作有些不同。

把全备文件备份一下，把数据库二进制日志也保存一下：【这一步为了二进制增备恢复做准备】

```
[root@test3 backup]# cp -r  full_backup full_backup_bak
[root@test3 backup]# cp /data/mysql/test3-bin.000001 /data/backup/
[root@test3 backup]# ls
full_backup  full_backup_bak  incre1  test3-bin.000001
```

恢复操作：

准备数据

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
第一准备全备：
[root@test3 ~]# innobackupex  --apply-log --redo-only --use-memory=1G /data/backup/full_backup--use-memory=1G #表示使用的内存大小第二步：把增量备份在全备上面回放，也就是把增量备份附加到全备上。[root@test3 ~]# innobackupex --apply-log --redo-only /data/backup/full_backup --incremental-dir=/data/backup/incre1整个恢复操作分为两步：1：准备全部备份，保证文件的一致性。2：把所有的增量备份附加到全备备份上。如果有多个增量备份，则每一个增量备份文件都要执行附加操作。但是要注意顺序，可以查看增量备份的xtrabackup_checkpoints文件 根据lsn来确定文件的顺序
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

开始恢复

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
[root@test3 ~]# service mysqld  stop
[root@test3 ~]# rm -fr /data/mysql/*
[root@test3 ~]# innobackupex --copy-back /data/backup/full_backup
[root@test3 ~]# chown -R mysql:mysql /data/mysql/#启动数据库，若是之前插入的两条数据存在，则说明增量备份恢复成功！[root@test3 ~]# service mysqld startStarting MySQL... SUCCESS! [root@test3 ~]# mysql -uroot -p123456mysql: [Warning] Using a password on the command line interface can be insecure.Welcome to the MySQL monitor.  Commands end with ; or \g.Your MySQL connection id is 2Server version: 5.7.22-log MySQL Community Server (GPL)Copyright (c) 2000, 2018, Oracle and/or its affiliates. All rights reserved.Oracle is a registered trademark of Oracle Corporation and/or itsaffiliates. Other names may be trademarks of their respectiveowners.Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.mysql> use sbtest;Reading table information for completion of table and column namesYou can turn off this feature to get a quicker startup with -ADatabase changedmysql> select * from tb3;           #增量备份插入的数据存在+------+---------------------+| a    | sysTime             |+------+---------------------+| aa   | 2018-11-29 09:14:00 || bb   | 2018-11-29 09:14:11 |+------+---------------------+2 rows in set (0.00 sec)
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

特别提示：

若有多个增量备份，则每一个增量备份都要重放到全备的文件中，一定要注意重放增量备份时候的顺序，可以在xtrabackup_checkpoints文件中根据lsn的大小来判断文件的顺序。

利用二进制日志做增量恢复

上面innobackupex的增量恢复，有点不方便，若是要恢复到某个时间点，显得有点无能为力了！但是二进制日志可以，二进制日志可以恢复指定时间点。

在上面的插入数据中，我们插入两条数据，但是一个要求，我们只要恢复到2018-11-29 09:14:00的数据，也就是插入的第二条数据不需要了！

我们上面备份了全备和二进制日志，现在就可以使用了！

基于全备恢复所有的数据，然后根据二进制日志把数据库恢复到我们要的时间点！

```
#执行全部备份的恢复
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
[root@test3 ~]#  innobackupex  --apply-log /data/backup/full_backup_bak[root@test3 ~]#  service mysqld stop
[root@test3 ~]#  rm -fr /data/mysql/
[root@test3 ~]#  innobackupex --copy-back /data/backup/full_backup_bak
[root@test3 ~]#  chown -R mysql:mysql
[root@test3 ~]#  chown -R mysql:mysql /data/mysql/
[root@test3 ~]#  service mysqld start
Starting MySQL.. SUCCESS! 
[root@test3 ~]# mysql -uroot -p123456     
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 2
Server version: 5.7.22-log MySQL Community Server (GPL)

Copyright (c) 2000, 2018, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> use sbtest;           #全备的时候，我们并没有在sbtest下面创建tb3这张表
Database changed
mysql> show tables;
Empty set (0.00 sec)
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

 \#执行基于二进制时间点的恢复

innobackupex备份的是，命令结束的时候的全部数据文件，可以在如下文件查看全备的时间信息：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
#有两种恢复方式基于时间点恢复[root@test3 mysql]# cat xtrabackup_info              
uuid = e44e6a70-f373-11e8-a5cd-fa336351fc00
name = 
tool_name = innobackupex
tool_command = --user=root --password=... --no-timestamp /data/backup/full_backup
tool_version = 2.4.4
ibbackup_version = 2.4.4
server_version = 5.7.22-log
start_time = 2018-11-29 09:12:17
end_time = 2018-11-29 09:12:51              #全部结束的时间
lock_time = 0
binlog_pos = filename 'test3-bin.000001', position '1782'
innodb_from_lsn = 0
innodb_to_lsn = 13741583205
partial = N
incremental = N
format = file
compact = N
compressed = N
encrypted = N#基于二进制的日志点恢复
[root@test3 mysql]# cat xtrabackup_binlog_pos_innodb   #全部备份结束的时间点
test3-bin.000001    1227
[root@test3 mysql]#
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

导出要恢复时间段的二进制日志文件：

我们要截取的日志时间段是从 全备结束 到 2018-11-29 09:14:00 这个时间点的文件。全备结束时间点作为起始点，起始点有两种确定方式，第一种上面文件中

的end_time指定的时间点；第二种二进制日志的position位置。这两种方式都可以。

 

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
[root@test3 mysql]# cd /data/backup/
[root@test3 backup]# mysqlbinlog --start-datetime="2018-11-29 09:12:51" --stop-datetime="2018-11-29 09:14:00" test3-bin.000001 > /root/bak.sql
--start-datetime ： 指定起始的时间点。
--stop-datetime：   指定结束的时间点。
--start-position：  指定二进制日志开始日志点。
--stop-position ：  指定二进制日志结束的日志点。
上面四个参数可以配合使用
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

 

然后把上面导出sql文件导入到MySQL服务中。

```
mysql> set sql_log_bin=0;          #导入时，若文件较大会产生很多二进制日志文件，可以先把二进制日志文件关闭，

mysql> source bak.sql;             #导入文件，可以使用mysql直接导入

mysql> set sql_log_bin=1;           #导入完成之后，开启二进制日志文件 
```

查看数据：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
mysql> use sbtest;   #完了演砸了，但是数据库已经存在了，说明这种方法是正确的！
Database changed
mysql> select * from tb3;
Empty set (0.01 sec)

mysql> show tables;
+------------------+
| Tables_in_sbtest |
+------------------+
| tb3              |
+------------------+
1 row in set (0.00 sec)
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

查看一下二进制日志：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
[root@test3 backup]# mysqlbinlog test3-bin.000001
.......
#181129  9:13:47 server id 5  end_log_pos 1973 CRC32 0xcad0188f     Query    thread_id=13    exec_time=0    error_code=0
SET TIMESTAMP=1543454027/*!*/;
create table tb3(a varchar(5), sysTime datetime)
/*!*/;
# at 1973        #用时间截取的时候很可能是截取到了这个时间点就停止了，因此只有tb3这个表，没有后面的插入的第一条数据，因此我们选择用日志点截取数据
#181129  9:14:00 server id 5  end_log_pos 2038 CRC32 0x670a3d93     Anonymous_GTID    last_committed=9    sequence_number=10    rbr_only=yes
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 2038
#181129  9:14:00 server id 5  end_log_pos 2120 CRC32 0x6a82446d     Query    thread_id=13    exec_time=0    error_code=0
SET TIMESTAMP=1543454040/*!*/;
BEGIN
/*!*/;
# at 2120
#181129  9:14:00 server id 5  end_log_pos 2172 CRC32 0x7c9e9e8c     Table_map: `sbtest`.`tb3` mapped to number 117
# at 2172
#181129  9:14:00 server id 5  end_log_pos 2216 CRC32 0xda4461be     Write_rows: table id 117 flags: STMT_END_F

BINLOG '
WD3/WxMFAAAANAAAAHwIAAAAAHUAAAAAAAEABnNidGVzdAADdGIzAAIPEgMFAAADjJ6efA==
WD3/Wx4FAAAALAAAAKgIAAAAAHUAAAAAAAEAAgAC//wCYWGZoXqTgL5hRNo=
'/*!*/;
# at 2216
#181129  9:14:00 server id 5  end_log_pos 2247 CRC32 0x3b63c077     Xid = 92
COMMIT/*!*/;
# at 2247            #选择这个日志点
#181129  9:14:11 server id 5  end_log_pos 2312 CRC32 0xbd634458     Anonymous_GTID    last_committed=10    sequence_number=11    rbr_only=yes
/*!50718 SET TRANSACTION ISOLATION LEVEL READ COMMITTED*//*!*/;
SET @@SESSION.GTID_NEXT= 'ANONYMOUS'/*!*/;
# at 2312
#181129  9:14:11 server id 5  end_log_pos 2394 CRC32 0xb5eba130     Query    thread_id=13    exec_time=0    error_code=0
SET TIMESTAMP=1543454051/*!*/;
BEGIN
/*!*/;
# at 2394
#181129  9:14:11 server id 5  end_log_pos 2446 CRC32 0xfcaf0343     Table_map: `sbtest`.`tb3` mapped to number 117
# at 2446
#181129  9:14:11 server id 5  end_log_pos 2490 CRC32 0x080cd871     Write_rows: table id 117 flags: STMT_END_F

BINLOG '
Yz3/WxMFAAAANAAAAI4JAAAAAHUAAAAAAAEABnNidGVzdAADdGIzAAIPEgMFAAADQwOv/A==
Yz3/Wx4FAAAALAAAALoJAAAAAHUAAAAAAAEAAgAC//wCYmKZoXqTi3HYDAg=
'/*!*/;
# at 2490
#181129  9:14:11 server id 5  end_log_pos 2521 CRC32 0x14fd7636     Xid = 93
COMMIT/*!*/;
SET @@SESSION.GTID_NEXT= 'AUTOMATIC' /* added by mysqlbinlog */ /*!*/;
DELIMITER ;
# End of log file
/*!50003 SET COMPLETION_TYPE=@OLD_COMPLETION_TYPE*/;
/*!50530 SET @@SESSION.PSEUDO_SLAVE_MODE=0*/;
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

 

用日志点重新导入一下：

```
mysqlbinlog --start-position=1227 --stop-position=2247 test3-bin.000001 > /root/bak.sql  #截取二进制日志
```

导入数据，查看数据：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
mysql> source bak.sql;mysql> use sbtest;
Database changed
mysql> show tables;
+------------------+
| Tables_in_sbtest |
+------------------+
| tb3              |
+------------------+
1 row in set (0.00 sec)

mysql> select * from tb3;          #数据已经存在
+------+---------------------+
| a    | sysTime             |
+------+---------------------+
| aa   | 2018-11-29 09:14:00 |
+------+---------------------+
1 row in set (0.00 sec)
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

 

 

增量备份有两种方法，一个是利用innobackupex的方式进行增量备份另一个是利用二进制日志进行增量备份；建议使用二进制日志增量备份！

我们公司的备份策略时，每一天凌晨全部备份，然后备份当天的二进制日志文件，这样就可以把数据恢复到任何一个时间点！



### 流压缩

上面备份的数据比较少，并没有考虑磁盘空间问题。下面我们来备份一个大库！

```
[root@test2 mariadb]# du -sh
47G    .
#datadir的数据文件时47G，好吧也不是太大，能说明问题了！
```

查看备份之后的文件大小：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
[root@test2 ~] innobackupex --user=root --password="123456" --no-timestamp --socket=/var/lib/mysql/mysql.sock  /test/backup/testdb
[root@test2 ~]# cd /test/backup/
[root@test2 backup]# ls
testdb
[root@test2 backup]# du -sh     #可以看到备份的文件也是47G
47G    .
[root@test2 backup]# 

#innobackupex是逻辑备份，就是拷贝数据，因此备份文件和源文件的大小相差不大
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

引入流压缩：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
[root@test2 backup]# innobackupex --user=root --password="7abec53701c3eefb" --no-timestamp --socket=/var/lib/mysql/mysql.sock --stream=tar /tmp |gzip -> testdb.tar.gz

--stram=tar  : 指定标准输出格式，一种是tar，另一种是xbstream；xbstream是xtraceback提供的，解压的时候需要专用命令处理，考虑通用性，选择用tar！

/tmp         : 流格式标准输出，临时存放于/tmp目录下面

| gzip ->    : 通过管道把流格式输出传给gzip进行压缩

#压缩备份完之后，查看一下压缩之后文件的大小：
[root@test2 backup]# ll -h testdb.tar.gz 
-rw-r--r-- 1 root root 18G Nov 29 11:27 testdb.tar.gz

压缩之前文件是47个G
未压缩的备份文件是47个G，
压缩之后备份文件是18个G。
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

之前的使用流压缩备份出来的命令，解压时必须带上 “-i” 参数！

```
[root@test3 backup]# tar -ixvf testdb.tar.gz -C db/        #之前的版本解压时候必须带上-i参数，否则会报错的！

-C ：直接把解压的文件放到指定的目录中，在恢复时，我们可以直接放到datadir指定的目录当中、

[root@test3 backup]# tar -zxvf testdb.tar.gz -C db/        #但是2.4版本直接使用zxvf解压也是可以的！percona优化了很多参数
```

文件解压之后，然后需要准备文件也就是需要使用--apply-log选项，之后进行恢复就可以了！恢复过程可以参照上面的恢复过程！

percona还有一种压缩方式，官方文档给出的就是这种压缩方式：[CLICK HERE！](https://www.percona.com/doc/percona-xtrabackup/2.4/howtos/recipes_ibkx_compressed.html)

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
#备份指令如下
[root@test2 ~]# innobackupex --user=root --password="7abec53701c3eefb" --socket="/var/lib/mysql/mysql.sock" --no-timestamp --compress --compress-threads=4  /test/backup/compress_db
--compress：开启压缩

--compress-threads=4 ： 使用4个压缩线程 

备份的文件如下：
[root@test2 backup]# cd compress_db       
[root@test2 compress_db]# ls       #文件如下
backup-my.cnf.qp  ib_buffer_pool.qp  mysql               xtrabackup_binlog_info.qp  xtrabackup_info.qp
financesys        ibdata1.qp         performance_schema  xtrabackup_checkpoints     xtrabackup_logfile.qp
[root@test2 compress_db]# du -sh   #文件大小为23G 
23G    .

#这个压缩文件，对表结构文件（frm）和表数据文件（ibd）都进行了压缩，压缩后的后缀名为 .qp!
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

在xtraceback2.1.4之前需要使用一个简短的shell来解压备份出来的文件，在2.1.4之后可以使用如下命令解压！

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
[root@test2 backup]# innobackupex --decompress /test/backup/compress_db/    
181129 13:55:50 innobackupex: Starting the decrypt and decompress operation

IMPORTANT: Please check that the decrypt and decompress run completes successfully.
           At the end of a successful decrypt and decompress run innobackupex
           prints "completed OK!".

innobackupex version 2.4.4 based on MySQL server 5.7.13 Linux (x86_64) (revision id: df58cf2)
181129 13:55:50 [01] decompressing ./ibdata1.qp
sh: qpress: command not found
cat: write error: Broken pipe
Error: thread 0 failed.
[root@test2 backup]# yum install -y qpress
Loaded plugins: fastestmirror
Setting up Install Process
Loading mirror speeds from cached hostfile
base                                                                                                                                                 | 3.7 kB     00:00     
epel                                                                                                                                                 | 4.4 kB     00:00     
mariadb                                                                                                                                              | 2.9 kB     00:00     
zabbix                                                                                                                                               | 2.9 kB     00:00     
No package qpress available.       #这里报错了没有qpress这个命令，这个命令需要从percona官网下载！网络有点渣，随后补上！
Error: Nothing to do
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

compress压缩备份之后的数据是23个G，而流压缩备份是18个G，源文件大小是47个G！根据存储来说建议使用流压缩吧！

 

\#备份指定的库，而不是全部的库！

![img](https://images.cnblogs.com/OutliningIndicators/ExpandedBlockStart.gif)

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
#!/bin/bash
#在备份指定库时，可以使用--include参数，也可以使用--tables-file参数，把指定的库写在文件中
User="root"
Hostip=x.x.x.x
Passwd="x.x.x.x.x.x"
Tbname="fi_repayplan.fi_reapyplan,fi_repayplan.fi_reapyplan_bank_once,fi_repayplan.fi_reapyplan_boc,fi_repayplan.fi_reapyplan_ccb,fi_repayplan.fi_reapyplan_irr,fi_repayplan.fi_reapyplan_irrmoney,fi_repayplan.fi_reapyplan_money,fi_repayplan.fi_reapyplan_once,fi_repayplan.fi_reapyplan_oncemoney"

innobackupex --user=${User} --password=${Passwd} --host=${Hostip} --no-timestamp --include=${Tbname} --stream=tar /tmp | gzip > /data/fi_repayplan-`date +%F`.tar.gz 
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

 

\#一个用于恢复的脚步

![img](https://images.cnblogs.com/OutliningIndicators/ExpandedBlockStart.gif)

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
#!/bin/badh
#Auth: wangxz
#需要把要解压的文件作为脚本执行的参数
#例如：sh huifu.sh xxx_2017-07-08_03-01-01.tar.gz
DB_DIR=/data/mariadb
DB_PORT=`netstat -lntp |grep 3306 | wc -l`
PROCESS_NUM=`ps aux |grep mysql |wc -l`
recover_log=/tmp/recover.log
print_str() ##打印说明信息
{
    echo -e "------------\e[0;31;1m $1 \e[0m-------------------"    
    sleep 1
}

check_ok()  ##一个检测函数
{
    if [ $? != 0 ]; then
        print_str "Some error has happened"
        exit
    else
        sleep 1
    fi
}
##检测是否给出要解压的文件
if [ $# !=  1 ]; then
    print_str "Please give a tar file"
    exit
fi
 
if [ $DB_PORT == 1 ] && [ $PROCESS_NUM == 3 ]; then
    print_str "It's stopping the mysql service"
    service mysql stop >> $recover_log
    check_ok
    sleep 3
fi

if [ $DB_PORT == 0 ] && [ $PROCESS_NUM == 1 ]; then
    print_str "The mysql service is not running!"
fi

###清楚datadir目录
print_str "Delete the datadir"
rm -fr $DB_DIR/*
check_ok
sleep 3

###解压数据库
print_str "Start Uncompress the backup file"
tar -ixvf $1 -C $DB_DIR/>> $recover_log
check_ok
sleep 3 

###准备数据库备份
print_str "apply log the datadir"
innobackupex --apply-log $DB_DIR  >> $recover_log
check_ok
sleep 3

###修改权限
chown -R mysql:mysql $DB_DIR
sleep 3

###启动数据库
service mysql start >>$recover_log
check_ok
###检测是否启动成功
PROCESSLIST=`ps aux |grep mysql |wc -l`
PORT_NUM=`netstat -lntp |grep 3306 | wc -l`
if [ $PROCESSLIST -gt 1 ] && [ $PORT_NUM -le 1 ]; then
    print_str "the mysql service has started successfully!"
    rm -fr $recover_log
    check_ok
    print_str "The log was deleted successfully!"
else
    print_str  "Some unexcept things has happened"
fi  
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)