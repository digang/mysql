# **mysql**从入门到放弃

## MySQL数据库介绍

MySQL 是一个关系型数据库管理系统，由瑞典 MySQL AB 公司开发，目前属于 Oracle 公司。MySQL 是一种关联数据库管理系统，关联数据库将数据保存在不同的表中，而不是将所有数据放在一个大仓库内，这样就增加了速度并提高了灵活性。

- MySQL 是开源的，目前隶属于 Oracle 旗下产品。
- MySQL 支持大型的数据库。可以处理拥有上千万条记录的大型数据库。
- MySQL 使用标准的 SQL 数据语言形式。
- MySQL 可以运行于多个系统上，并且支持多种语言。这些编程语言包括 C、C++、Python、Java、Perl、PHP、Eiffel、Ruby 和 Tcl 等。
- MySQL 对PHP有很好的支持，PHP 是目前最流行的 Web 开发语言。
- MySQL 支持大型数据库，支持 5000 万条记录的数据仓库，32 位系统表文件最大可支持 4GB，64 位系统支持最大的表文件为8TB。
- MySQL 是可以定制的，采用了 GPL 协议，你可以修改源码来开发自己的 MySQL 系统。



### 一、Ubuntu18.04 安装MySQL

环境信息：
OS：Ubuntu18.04
Server version: 5.7.36


#### 1.安装MySQL
##### 1.1 物理机或虚拟机上安装Mysql

在Ubuntu中，默认情况下，只有最新版本的MySQL包含在APT软件包存储库中,要安装它，只需更新服务器上的包索引并安装默认包apt-get。

```
apt update -y
apt-get install mysql-server
```
##### 1.2 docker安装Mysql

Docker是DotCloud开源的、可以将任何应用包装在Linux container中运行的工具

```bash
sudo apt install -y docker.io
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://li8f2khr.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker

# 拉取镜像
sudo docker pull mysql:5.7
# 启动mysql镜像并查看启动日志
sudo docker run -p 3307:3306 --name mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.7 && sudo docker logs mysql
# 进入mysql所在镜像实例的终端中，然后在终端就可以使用：mysql -u root -p 输入密码123456登录
sudo docker exec -it mysql bash
# 启动/停止mysql实例 
sudo docker start/stop mysql 
# Ubuntu下设置开启自启
sudo systemctl enable docker
# 查看开机自启应用
systemctl list-unit-files | grep enabled
```



#### 2.mysql数据库配置

##### 2.1 初始化配置
```
sudo mysql_secure_installation
```
##### 2.2 检查mysql服务状态
```
systemctl status mysql.service
```
#### 3 配置用户访问权限
在Ubuntu下MySQL缺省是只允许本地访问的
```mysql
#登录数据库
mysql -p -uroot
Enter password:

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
4 rows in set (0.00 sec)

mysql> use mysql
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
#查看mysql库下的所有表
mysql> show tables;
mysql> SELECT USER();
+----------------+
| USER()         |
+----------------+
| root@localhost |
+----------------+
1 row in set (0.00 sec)
#查看数据库变量
mysql> SHOW VARIABLES;
#查看表字段
mysql> desc user;
mysql> SHOW CREATE TABLE user;
#创建数据库
mysql> create database mydb;
#创建表
mysql> CREATE TABLE name (name varchar(100) NOT NULL,age int(8));
#插入数据
mysql> insert into name set name='Tony',age=20;
#插入多条记录
mysql> insert into name  values('jack',23),('lee',25);
#更新一条记录
update name set age=25 where name='Tony';
#添加用户
mysql> create user yangxp identified by 'yangxp@123';
#查看用户
mysql> select Host, User, plugin,authentication_string from user;
+-----------+------------------+-----------------------+-------------------------------------------+
| Host      | User             | plugin                | authentication_string                     |
+-----------+------------------+-----------------------+-------------------------------------------+
| localhost | root             | auth_socket           | *967F3DD3F5B4E829F3D23C01BE94C96880D7447D |
| localhost | mysql.session    | mysql_native_password | *967F3DD3F5B4E829F3D23C01BE94C96880D7447D |
| localhost | mysql.sys        | mysql_native_password | *967F3DD3F5B4E829F3D23C01BE94C96880D7447D |
| localhost | debian-sys-maint | mysql_native_password | *D7B66DC3496A0B1F4370F59718EF820E26AC8DF4 |
| %         | yangxp           | mysql_native_password | *A509E632F3B31F6A9E61654D2FFD3E7A315B7770 |
+-----------+------------------+-----------------------+-------------------------------------------+
#授权

mysql> grant all privileges on mydb.* to yangxp@'%' identified by 'yangxp@123';
mysql> flush privileges;
#上面的语句将mydb数据库的所有操作权限都授权给了用户yangxp。
#修改密码
mysql>alter user 'root'@'localhost' identified with mysql_native_password by 'Yangxp@123';

mysql> update mysql.user set authentication_string = password('Yangxp@123') where user = 'root';

mysql>flush privileges;

#删除用户命令
drop user user_name@'%';
#查看mysql进程列表
#show processlist

```
#### 4. 常用命令组

**创建用户并授予指定数据库全部权限：适用于Web应用创建MySQL用户**

```mysql
create user yangxp identified by 'yangxp';
grant all privileges on mydb.* to yangxp@'%' identified by 'yangxp@123';
flush  privileges;
```

创建了用户yangxp，并将数据库mydb的所有权限授予yangxp。如果要使yangxp可以从本机登录，那么可以多赋予localhost权限：

```mysql
grant all privileges on mydb.* to yangxp@'localhost' identified by 'yangxp@123';
```

**privilegesCode**表示授予的权限类型，常用的有以下几种类型[1]：

- all privileges：所有权限。
- select：读取权限。
- delete：删除权限。
- update：更新权限。
- create：创建权限。
- drop：删除数据库、数据表权限。

**dbName.tableName**表示授予权限的具体库或表，常用的有以下几种选项：

- *.*：授予该数据库服务器所有数据库的权限。
- dbName.*：授予dbName数据库所有表的权限。
- dbName.dbTable：授予数据库dbName中dbTable表的权限。

`username@host`表示授予的用户以及允许该用户登录的IP地址。其中Host有以下几种类型：

- **localhost：**只允许该用户在本地登录，不能远程登录。
- **%：**允许在除本机之外的任何一台机器远程登录。
- **192.168.1.10：**具体的IP表示只允许该用户从特定IP登录。

**password**指定该用户登录时的面。

**flush privileges**表示刷新权限变更
### 二、 mysql数据库备份和还原

#### 1.数据备份

##### 1.1、热备份

使用mysqldump命令备份

　　mysqldump命令将数据库中的数据备份成一个文本文件。表的结构和表中的数据将存储在生成的文本文件中。

　　mysqldump命令的工作原理很简单。它先查出需要备份的表的结构，再在文本文件中生成一个CREATE语句。然后，将表中的所有记录转换成一条INSERT语句。然后通过这些语句，就能够创建表并插入数据。

　　**1、备份一个数据库**

　　mysqldump基本语法：

　　*mysqldump -u username -p dbname table1 table2 ...-> BackupName.sql*

　　其中：

- dbname参数表示数据库的名称；
- table1和table2参数表示需要备份的表的名称，为空则整个数据库备份；
- BackupName.sql参数表设计备份文件的名称，文件名前面可以加上一个绝对路径。通常将数据库被分成一个后缀名为sql的文件；

　　使用root用户备份test数据库下的person表

```
mysqldump -u root -p test person > /root/backup.sql
```

文件的开头会记录MySQL的版本、备份的主机名和数据库名。

　　文件中以“--”开头的都是SQL语言的注释，以"/*!40101"等形式开头的是与MySQL有关的注释。40101是MySQL数据库的版本号，如果MySQL的版本比1.11高，则/*!40101和*/之间的内容就被当做SQL命令来执行，如果比4.1.1低就会被当做注释。

　　**2、备份多个数据库**

　　语法：

```mysql
mysqldump -u username -p --databases dbname2 dbname2 > Backup.sql
```

　　加上了--databases选项，然后后面跟多个数据库

```mysql
mysqldump -u root -p --databases test mysql > /root/backup.sql
```

　　**3、备份所有数据库**

　　mysqldump命令备份所有数据库的语法如下：

```mysql
mysqldump -u username -p -all-databases > BackupName.sql

其他导出
1.导出结构不导出数据 
mysqldump　--opt　-d　数据库名　-u　root　-p　>　xxx.sql　　 
2.导出数据不导出结构 
mysqldump　-t　数据库名　-uroot　-p　>　xxx.sql　 
3.导出数据和表结构 
mysqldump　数据库名　-uroot　-p　>　xxx.sql　 
4.导出特定表的结构
mysqldump　-uroot　-p　-B　数据库名　--table　表名　>　xxx.sql　　 

```

　　**4、其他备份mysql软件**
　　1.mysqlhotcopy  目前，该工具也仅仅能够备份MyISAM类型的表
　　语法示例：

```mysql
　　mysqlhotcopy [option] dbname1 dbname2 backupDir/
```
　　2.percona Xtrabackup 备份工具

Xtrabackup是由percona开发的一个开源软件，它是innodb热备工具ibbackup（收费的商业软件）的一个开源替代品。Xtrabackup由个部分组成:xtrabackup和innobackupex，其中xtrabackup工具用于备份innodb和 xtraDB引擎的表；而innobackupex工具用于备份myisam和innodb引擎的表

　　语法示例:

```
　#安装
　apt-get install percona-xtrabackup
　
 #全备份操作 （throttle – 限制每秒的IOS数量，以避免瞬间对服务器性能产生较大影响）
　xtrabackup --target-dir=/data/20211027/ --backup --throttle=100
　　
　#备份很简单，/data/backup/为备份的目录
　innobackupex --user=root --password=123456  /data/backup/          
```

　　#增量备份操作



```
xtrabackup --backup --throttle=100 \
--target-dir=/data/20211027/20211027/ \
--incremental-basedir=/data/20211027 \

innobackupex --user=root  --no-timestamp --incremental /data/backup/incre1 --incremental-basedir=/data/backup/full_backup
```
##### 2、冷备份
直接复制整个数据库目录

　　MySQL有一种非常简单的备份方法，就是将MySQL中的数据库文件直接复制出来。这是最简单，速度最快的方法。

不过在此之前，要先将服务器停止，这样才可以保证在复制期间数据库的数据不会发生变化。如果在复制数据库的过程中还有数据写入，就会造成数据不一致。这种情况在开发环境可以，但是在生产环境中很难允许备份服务器。

　　注意：这种方法不适用于InnoDB存储引擎的表，而对于MyISAM存储引擎的表很方便。同时，还原时MySQL的版本最好相同。

　　

#### 2.数据还原

##### 2.1、mysql命令
 使用mysql命令备份的数据库的语法如下：
　　示例：

```mysql
mysql -u root -p dbname < /root/backup.sql
```

或者登录mysql使用source导入

```mysql
mysql> /root/backup.sql
```

##### 2.2、还原直接复制目录的备份

　　通过这种方式还原时，必须保证两个MySQL数据库的版本号是相同的。MyISAM类型的表有效，对于InnoDB类型的表不可用，InnoDB表的表空间不能直接复制。

##### 2.3、xtrabackup的恢复数据方法

xtrabackup恢复分为两个步骤：

· 准备恢复：所谓准备恢复，就是要为恢复做准备。就是说备份集没办法直接拿来用，因为这中间可能存在未提交或未回滚的事务，数据文件不一致，所以需要一个队备份集的准备过程。

```shell
#准备恢复
innobackupex --apply-log /data/backup/2018-11-28_18-50-47/
```

· 执行恢复：innobackupex提供了--copy-back参数，就是将指定的备份集，恢复到指定的路径下面（这个指定的路径是配置文件中datadir指定的路径）

```shell
#执行恢复
#先停止数据库
service mysql stop
rm -fr /data/mysql/*      #情况datadir指定的目录，若是线上环境，确保一定备份过
#恢复命令
innobackupex --copy-back /data/backup/2018-11-28_18-50-47
#修改所属用户
chown -R mysql:mysql /data/mysql/
service mysql start
```

一次mysql删除数据文件，没备份的 恢复记录

https://blog.csdn.net/mo18850223090/article/details/105646561



### 三、mysql主从同步

#### 3.1 什么是mysql 的主从复制？

​        指一台服务器充当主数据库服务器，另一台或多台服务器充当从数据库服务器，主服务器中的数据自动复制到从服务器之中。对于多级复制，数据库服务器即可充当主机，也可充当从机。MySQL主从复制的基础是主服务器对数据库修改记录二进制日志，从服务器通过主服务器的二进制日志自动执行更新。
 一句话表示就是，主数据库做什么，从数据库就跟着做什么。

#### 3.2 mysql复制的类型

1.基于语句的复制 ：主库把sql语句写入到bin log中，完成复制
2.基于行数据的复制：主库把每一行数据变化的信息作为事件，写入到bin log，完成复制
3.混合复制：上面两个结合体，默认用语句复制，出问题时候自动切换成行数据复制
tip:和上面相对应的日志格式也有三种:STATEMENT，ROW，MIXED。

1.STATEMENT模式（SBR）
    每一条会修改数据的sql语句会记录到binlog中。优点是并不需要记录每一条sql语句和每一行的数据变化，减少了binlog日志量，节约IO，提高性能。缺点是在      某些情况下会导致master-slave中的数据不一致(如sleep()函数， last_insert_id()，以及user-defined functions(udf)等会出现问题)

2.ROW模式（RBR）
不记录每条sql语句的上下文信息，仅需记录哪条数据被修改了，修改成什么样了。而且不会出现某些特定情况下的存储过程、或function、或trigger的调用和触发无法被正确复制的问题。缺点是会产生大量的日志，尤其是alter table的时候会让日志暴涨。

3.MIXED模式（MBR）
以上两种模式的混合使用，一般的复制使用STATEMENT模式保存binlog，对于STATEMENT模式无法复制的操作使用ROW模式保存binlog，MySQL会根据执行的SQL语句选择日志保存方式。

#### 3.3 主从复制工作原理剖析

1.Master 数据库只要发生变化，立马记录到Binary log 日志文件中
 2.Slave数据库启动一个I/O thread连接Master数据库，请求Master变化的二进制日志
 3.Slave I/O获取到的二进制日志，保存到自己的Relay log 日志文件中。
 4.Slave 有一个 SQL thread定时检查Realy log是否变化，变化那么就更新数据



![img](https://upload-images.jianshu.io/upload_images/5009863-865d52a5f616a52d.jpg?imageMogr2/auto-orient/strip|imageView2/2/w/506/format/webp)



### 3.4 为什么要用mysql 的主从

1.**实现服务器负载均衡**
    即可以通过在主服务器和从服务器之间切分处理客户查询的负荷，从而得到更好的客户相应时间。通常情况下，数据库管理员会有两种思路。

​    一是在主服务器上只实现数据的更新操作。包括数据记录的更新、删除、新建等等作业。而不关心数据的查询作业。数据库管理员将数据的查询请求全部 转发到从服务器中。这在某些应用中会比较有用。如某些应用，像基金净值预测的网站。其数据的更新都是有管理员更新的，即更新的用户比较少。而查询的用户数 量会非常的多。此时就可以设置一台主服务器，专门用来数据的更新。同时设置多台从服务器，用来负责用户信息的查询

​    二是在主服务器上与从服务器切分查询的作业。在这种思路下，主服务器不单单要完成数据的更新、删除、插入等作业，同时也需要负担一部分查询作业。而从服务器的话，只负责数据的查询。当主服务器比较忙时，部分查询请求会自动发送到从服务器重，以降低主服务器的工作负荷。

2.**通过复制实现数据的异地备份**
    可以定期的将数据从主服务器上复制到从服务器上，这无疑是先了数据的异地备份。在传统的备份体制下，是将数据备份在本地。此时备份 作业与数据库服务器运行在同一台设备上，当备份作业运行时就会影响到服务器的正常运行。有时候会明显的降低服务器的性能。同时，将备份数据存放在本地，也 不是很安全。如硬盘因为电压等原因被损坏或者服务器被失窃，此时由于备份文件仍然存放在硬盘上，数据库管理员无法使用备份文件来恢复数据。这显然会给企业 带来比较大的损失。

3.**提高数据库系统的可用性**

​    数据库复制功能实现了主服务器与从服务器之间数据的同步，增加了数据库系统的可用性。当主服务器出现问题时，数据库管理员可以马上让从服务器作为主服务器，用来数据的更新与查询服务。然后回过头来再仔细的检查主服务器的问题。此时一般数据库管理员也会采用两种手段。

​    一是主服务器故障之后，虽然从服务器取代了主服务器的位置，但是对于主服务器可以采取的操作仍然做了一些限制。如仍然只能够进行数据的查询，而 不能够进行数据的更新、删除等操作。这主要是从数据的安全性考虑。如现在一些银行系统的升级，在升级的过程中，只能够查询余额而不能够取钱。这是同样的道 理。

​    二是从服务器真正变成了主服务器。当从服务器切换为主服务器之后，其地位完全与原先的主服务器相同。此时可以实现对数据的查询、更新、删除等操 作。为此就需要做好数据的安全性工作。即数据的安全策略，要与原先的主服务器完全相同。否则的话，就可能会留下一定的安全隐患

#### 3.5 怎么配置mysql主从复制

##### 3.5.1 环境

本地安装两个mysql，或者使用虚拟机，或者使用docker安装，需要准备两个mysql

Ubuntu 18.4

mysql:5.7.36

master-node1:192.168.7.189    部署mysql1

slave-node2:192.168.7.190    部署mysql2



##### 3.5.2 配置文件

mysql1(master): 192.168.7.189  my.cnf 配置文件设置

```
#mysql master-node1 config 
[mysqld]
server-id = 1        # 节点ID，确保唯一

# log config
log-bin = mysql-bin     #开启mysql的binlog日志功能
sync_binlog = 1         #控制数据库的binlog刷到磁盘上去 , 0 不控制，性能最好，1每次事物提交都会刷到日志文件中，性能最差，最安全
binlog_format = mixed   #binlog日志格式，mysql默认采用statement，建议使用mixed
expire_logs_days = 7                           #binlog过期清理时间
max_binlog_size = 100m                    #binlog每个日志文件大小
binlog_cache_size = 4m                        #binlog缓存大小
max_binlog_cache_size= 512m              #最大binlog缓存大
binlog-ignore-db=mysql #不生成日志文件的数据库，多个忽略数据库可以用逗号拼接，或者 复制这句话，写多行

auto-increment-offset = 1     # 自增值的偏移量
auto-increment-increment = 1  # 自增值的自增量
slave-skip-errors = all #跳过从库错误
bind-address = 0.0.0.0
```

mysql2(slave): 192.168.7.190 mysql.cnf 配置

```
#mysql slave-node2 config
[mysqld]
server-id = 2
log-bin=mysql-bin
relay-log = mysql-relay-bin
replicate-wild-ignore-table=mysql.%
replicate-wild-ignore-table=test.%
replicate-wild-ignore-table=information_schema.%
```

重启两个mysql，让配置生效

##### 3.5.3 master数据库，创建复制用户并授权

1.进入master的数据库，为master创建复制用户

```mysql
CREATE USER repl_user IDENTIFIED BY 'Repl_123';
```

2.赋予该用户复制的权利

```mysql
grant replication slave on *.* to 'repl_user'@'192.168.7.190'  identified by 'Repl_123';

FLUSH PRIVILEGES;
```

3.查看master的状态



```mysql
mysql> show master status;
+------------------+----------+--------------+------------------+-------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+------------------+----------+--------------+------------------+-------------------+
| mysql-bin.000001 |      154 |              | mysql            |                   |
+------------------+----------+--------------+------------------+-------------------+
```

4,配置从库



```mysql
mysql> CHANGE MASTER TO 
MASTER_HOST = '192.168.7.189',  
MASTER_USER = 'repl_user', 
MASTER_PASSWORD = 'Repl_123',
MASTER_PORT = 3306,
MASTER_LOG_FILE='mysql-bin.000001',
MASTER_LOG_POS=1008,
MASTER_RETRY_COUNT = 60,
MASTER_HEARTBEAT_PERIOD = 10000; 

# MASTER_LOG_FILE='mysql-bin.000005',#与主库File 保持一致
# MASTER_LOG_POS=120 , #与主库Position 保持一致
```

启动从库slave进程



```mysql
mysql> start slave;
Query OK, 0 rows affected (0.04 sec)
```

查看是否配置成功

```mysql
mysql> show slave status\G
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 192.168.7.189
                  Master_User: repl_user
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000002
          Read_Master_Log_Pos: 154
               Relay_Log_File: mysql-relay-bin.000004
                Relay_Log_Pos: 367
        Relay_Master_Log_File: mysql-bin.000002
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
           Replicate_Do_Table: 
       Replicate_Ignore_Table: 
      Replicate_Wild_Do_Table: 
  Replicate_Wild_Ignore_Table: mysql.%,test.%,information_schema.%
                   Last_Errno: 0
                   Last_Error: 
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 154
              Relay_Log_Space: 740
              Until_Condition: None
               Until_Log_File: 
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File: 
           Master_SSL_CA_Path: 
              Master_SSL_Cert: 
            Master_SSL_Cipher: 
               Master_SSL_Key: 
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error: 
               Last_SQL_Errno: 0
               Last_SQL_Error: 
  Replicate_Ignore_Server_Ids: 
             Master_Server_Id: 1
                  Master_UUID: e8956511-37a0-11ec-b379-000c29f1fcf1
             Master_Info_File: /var/lib/mysql/master.info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
      Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
           Master_Retry_Count: 60
                  Master_Bind: 
      Last_IO_Error_Timestamp: 
     Last_SQL_Error_Timestamp: 
               Master_SSL_Crl: 
           Master_SSL_Crlpath: 
           Retrieved_Gtid_Set: 
            Executed_Gtid_Set: 
                Auto_Position: 0
         Replicate_Rewrite_DB: 
                 Channel_Name: 
           Master_TLS_Version: 
1 row in set (0.00 sec)
```



