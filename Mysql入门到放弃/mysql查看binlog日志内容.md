# mysql查看binlog日志内容

## （一） binlog介绍

binlog,即二进制日志,它记录了数据库上的所有改变，并以二进制的形式保存在磁盘中；

它可以用来查看数据库的变更历史、数据库增量备份和恢复、Mysql的复制（主从数据库的复制）。

##  （二） binlog格式

binlog有三种格式：Statement、Row以及Mixed。

–基于SQL语句的复制(statement-based replication,SBR)， 
–基于行的复制(row-based replication,RBR)， 
–混合模式复制(mixed-based replication,MBR)。

2.1 Statement 
每一条会修改数据的sql都会记录在binlog中。

优点：不需要记录每一行的变化，减少了binlog日志量，节约了IO，提高性能。

缺点：由于记录的只是执行语句，为了这些语句能在slave上正确运行，因此还必须记录每条语句在执行的时候的一些相关信息，以保证所有语句能在slave得到和在master端执行时候相同 的结果。另外mysql 的复制,像一些特定函数功能，slave可与master上要保持一致会有很多相关问题。

ps：相比row能节约多少性能与日志量，这个取决于应用的SQL情况，正常同一条记录修改或者插入row格式所产生的日志量还小于Statement产生的日志量，但是考虑到如果带条件的update操作，以及整表删除，alter表等操作，ROW格式会产生大量日志，因此在考虑是否使用ROW格式日志时应该跟据应用的实际情况，其所产生的日志量会增加多少，以及带来的IO性能问题。

2.2 Row

5.1.5版本的MySQL才开始支持row level的复制,它不记录sql语句上下文相关信息，仅保存哪条记录被修改。

优点： binlog中可以不记录执行的sql语句的上下文相关的信息，仅需要记录那一条记录被修改成什么了。所以rowlevel的日志内容会非常清楚的记录下每一行数据修改的细节。而且不会出现某些特定情况下的存储过程，或function，以及trigger的调用和触发无法被正确复制的问题.

缺点:所有的执行的语句当记录到日志中的时候，都将以每行记录的修改来记录，这样可能会产生大量的日志内容。

ps:新版本的MySQL中对row level模式也被做了优化，并不是所有的修改都会以row level来记录，像遇到表结构变更的时候就会以statement模式来记录，如果sql语句确实就是update或者delete等修改数据的语句，那么还是会记录所有行的变更。

2.3 Mixed

从5.1.8版本开始，MySQL提供了Mixed格式，实际上就是Statement与Row的结合。

在Mixed模式下，一般的语句修改使用statment格式保存binlog，如一些函数，statement无法完成主从复制的操作，则采用row格式保存binlog，MySQL会根据执行的每一条具体的sql语句来区分对待记录的日志形式，也就是在Statement和Row之间选择一种。

只查看第一个binlog文件的内容
mysql> show binlog events;

查看指定binlog文件的内容
mysql> show binlog events in 'mysql-bin.000001';

![img](https://img-blog.csdn.net/20180107182709022?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvdTAxMDAwMjE4NA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)获取binlog文件列表

mysql> show binary logs;

**[python]** [view plain](http://blog.csdn.net/nuli888/article/details/52106910#) [copy](http://blog.csdn.net/nuli888/article/details/52106910#)

1. mysql> show binary logs; 
2. +------------------+-----------+ 
3. | Log_name     | File_size | 
4. +------------------+-----------+ 
5. | mysql-bin.000001 |   3548 | 
6. | mysql-bin.000002 |    106 | 
7. +------------------+-----------+ 
8. 2 rows **in** set (0.00 sec) 
9. 



1 当停止或重启服务器时，服务器会把日志文件记入下一个日志文件，Mysql会在重启时生成一个新的日志文件，文件序号递增；

2 如果日志文件超过max_binlog_size（默认值1G）系统变量配置的上限时，也会生成新的日志文件（在这里需要注意的是，如果你正使用大的事务，二进制日志还会超过max_binlog_size，不会生成新的日志文件，事务全写入一个二进制日志中,这种情况主要是为了保证事务的完整性）

3 日志被刷新时，新生成一个日志文件。

如：

![img](https://img-blog.csdn.net/20180107182835815?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvdTAxMDAwMjE4NA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)



binglog的查看
通过mysqlbinlog命令可以查看binlog的内容
[root@localhost ~]# mysqlbinlog /home/mysql/binlog/binlog.000003 | more

/*!40019 SET @@session.max_insert_delayed_threads=0*/;
/*!50003 SET @OLD_COMPLETION_TYPE=@@COMPLETION_TYPE,COMPLETION_TYPE=0*/;
DELIMITER /*!*/;
\# at 4
\#120330 16:51:46 server id 1 end_log_pos 98  Start: binlog v 4, server v 5.0.45-log created 120330 1
6:51:46
\# Warning: this binlog was not closed properly. Most probably mysqld crashed writing it.
\# at 196
\#120330 17:54:15 server id 1 end_log_pos 294  Query  thread_id=3   exec_time=2   error_code=0
SET TIMESTAMP=1333101255/*!*/;
insert into tt7 select * from tt7/*!*/;
\# at 294
\#120330 17:54:46 server id 1 end_log_pos 388  Query  thread_id=3   exec_time=28  error_code=0
SET TIMESTAMP=1333101286/*!*/;
alter table tt7 engine=innodb/*!*/;



解析binlog格式

位置
位于文件中的位置，“at 294”说明“事件”的起点，是以第294字节开始；“end_log_pos 388 ”说明以第388 字节结束



时间戳
事件发生的时间戳：“120330 17:54:46”



事件执行时间
事件执行花费的时间:"exec_time=28"



错误码
错误码为：“error_code=0”



服务器的标识
服务器的标识id：“server id 1”