

#### 星云监控端数据库的常用操作



```
root@gucloud:~# docker ps -a
CONTAINER ID   IMAGE                                                              COMMAND                  CREATED         STATUS       PORTS                                                                           NAMES
6f55c21d43c8   registry.cn-shanghai.aliyuncs.com/nebula_cloud/nebula:v0.30.0      "sh -c '/wait && /op…"   4 days ago      Up 4 days    172.16.0.215:10086->10086/tcp, 0.0.0.0:18001->18001/tcp                         nebula_local
87dfd09a12da   registry.cn-shanghai.aliyuncs.com/nebula_cloud/rabbitmq:latest     "docker-entrypoint.s…"   6 months ago    Up 8 weeks   4369/tcp, 5671/tcp, 25672/tcp, 172.16.0.215:5672->5672/tcp                      rabbitmq
407cbeba53f3   registry.cn-shanghai.aliyuncs.com/nebula_cloud/tool:latest         "/usr/local/bin/pyth…"   7 months ago    Up 8 weeks                                                                                   tool
fcf6b6cf6afd   registry.cn-shanghai.aliyuncs.com/nebula_cloud/consul:latest       "docker-entrypoint.s…"   7 months ago    Up 8 weeks   8300-8302/tcp, 8301-8302/udp, 8600/tcp, 8600/udp, 172.16.0.215:8500->8500/tcp   consul
564541e13aa0   registry.cn-shanghai.aliyuncs.com/nebula_cloud/salt:latest         "/usr/bin/dumb-init …"   7 months ago    Up 4 days    172.16.0.215:4505-4506->4505-4506/tcp, 8000/tcp                                 salt
7f870fa6fa72   registry.cn-shanghai.aliyuncs.com/nebula_cloud/loki:latest         "/usr/bin/loki -conf…"   7 months ago    Up 8 weeks   172.16.0.215:3100->3100/tcp                                                     loki
7d18d64b38f0   registry.cn-shanghai.aliyuncs.com/nebula_cloud/grafana:master      "/run.sh"                7 months ago    Up 8 weeks   3000/tcp                                                                        grafana
de731330ed28   registry.cn-shanghai.aliyuncs.com/nebula_cloud/prometheus:latest   "/bin/prometheus --w…"   10 months ago   Up 8 weeks   0.0.0.0:9090->9090/tcp                                                          prometheus
9e25bd2c2829   registry.cn-shanghai.aliyuncs.com/nebula_cloud/mysql:latest        "docker-entrypoint.s…"   10 months ago   Up 6 weeks   3306/tcp, 33060/tcp
```



#进入mysql容器

```bash
docker exec -it mysql /bin/bash

mysql -uroot -p nebula_local
```



#监控端释放勾选框

```mysql
docker exec -it mysql /bin/bash
mysql -p
use nebula_local
mysql> update node set usable=1  where usable=0;
```

```shell
#直接在shell执行
docker exec -i mysql sh -c 'mysql -uroot -proot123456 nebula_local -e "update node set usable=1  where usable=0;"'
```

```mysql
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| bak                |
| mysql              |
| nebula_local       |
| performance_schema |
| sys                |
+--------------------+
6 rows in set (0.00 sec)
mysql> use nebula_local 
Database changed
mysql> show tables;
+------------------------------+
| Tables_in_nebula_local       |
+------------------------------+
| action_state                 |
| alarm_level                  |
| alarm_node_health            |
| alarm_node_health_state      |
| alarm_notify_state           |
| alarm_phone                  |
| alarm_sector_state           |
| alarm_task_status            |
| casbin_rule                  |
| cpu_mode_config              |
| dev_ops                      |
| interface_permission         |
| job                          |
| lotus_chain_bk               |
| lotus_fault_sector           |
| lotus_msg                    |
| lotus_order                  |
| lotus_pre_order              |
| lotus_removed_sector         |
| lotus_sector                 |
| lotus_sector_state           |
| lotus_sector_task            |
| lotus_transfer_log           |
| lotus_version                |
| lotus_wallet                 |
| lotus_window_post            |
| lotus_window_post_batch      |
| lotus_winning_post           |
| lotus_winning_post_detail    |
| lotus_withdraw_account       |
| menu_permission              |
| miner_backup                 |
| miner_wallet                 |
| monitor                      |
| node                         |
| node_chain                   |
| node_disk                    |
| node_group                   |
| node_group_cfg_sector_scalp  |
| node_group_cfg_withdraw      |
| node_group_cfg_withdraw_item |
| node_group_message           |
| node_health                  |
| node_seal_worker_c2          |
| node_seal_worker_p1          |
| node_seal_worker_p1_p2_c1    |
| node_seal_worker_p2          |
| node_seal_worker_post        |
| node_storage_miner           |
| nodes_health_setting         |
| on_chain_infos_sync_record   |
| report_day_winning_post      |
| res_act                      |
| role                         |
| role_op_log                  |
| sandstone_bucket             |
| sector_extend                |
| sector_extend_config         |
| sector_extend_log            |
| sys_setting                  |
| task_warning_config_group    |
| trace_log                    |
| url_permission               |
| user                         |
| user_config                  |
| user_group                   |
| user_monitor                 |
+------------------------------+
67 rows in set (0.00 sec)

mysql>
```

```mysql
#查看mysql当前进程
mysql> show processlist;
+----------+------+------------------+--------------+---------+------+----------+------------------+
| Id       | User | Host             | db           | Command | Time | State    | Info             |
+----------+------+------------------+--------------+---------+------+----------+------------------+
| 12014387 | root | localhost        | nebula_local | Query   |    0 | starting | show processlist |
| 12016343 | root | 172.19.0.2:55738 | nebula_local | Sleep   |    9 |          | NULL             |
| 12016344 | root | 172.19.0.2:55740 | nebula_local | Sleep   |    9 |          | NULL             |
| 12016345 | root | 172.19.0.2:55742 | nebula_local | Sleep   |    9 |          | NULL             |
| 12016346 | root | 172.19.0.2:55746 | nebula_local | Sleep   |    9 |          | NULL             |
| 12016348 | root | 172.19.0.2:55750 | nebula_local | Sleep   |   11 |          | NULL             |
| 12016350 | root | 172.19.0.2:55754 | nebula_local | Sleep   |    9 |          | NULL             |
| 12016351 | root | 172.19.0.2:55756 | nebula_local | Sleep   |   11 |          | NULL             |
| 12016352 | root | 172.19.0.2:55758 | nebula_local | Sleep   |    9 |          | NULL             |
| 12016353 | root | 172.19.0.2:55760 | nebula_local | Sleep   |    9 |          | NULL             |
| 12016354 | root | 172.19.0.2:56068 | nebula_local | Sleep   |    1 |          | NULL             |
+----------+------+------------------+--------------+---------+------+----------+------------------+
11 rows in set (0.00 sec)
```

杉岩存储桶配置 sandstone_bucket表

```mysql
mysql> desc sandstone_bucket;
+---------------+----------------------+------+-----+---------+----------------+
| Field         | Type                 | Null | Key | Default | Extra          |
+---------------+----------------------+------+-----+---------+----------------+
| id            | int(10) unsigned     | NO   | PRI | NULL    | auto_increment |
| name          | varchar(256)         | NO   | MUL |         |                |
| group_id      | int(10) unsigned     | NO   |     | 0       |                |
| miner_node_id | varchar(256)         | NO   |     |         |                |
| miner_name    | varchar(256)         | NO   |     |         |                |
| domain_host   | varchar(256)         | NO   |     |         |                |
| domain        | varchar(256)         | NO   |     |         |                |
| placement     | varchar(256)         | NO   |     |         |                |
| capacity      | smallint(5) unsigned | NO   |     | 0       |                |
| access_key    | varchar(64)          | NO   |     |         |                |
| secret_key    | varchar(64)          | NO   |     |         |                |
| ctime         | int(10) unsigned     | NO   |     | 0       |                |
| mtime         | int(10) unsigned     | YES  |     | 0       |                |
| om_created    | tinyint(3) unsigned  | YES  |     | 0       |                |
| om_user_id    | varchar(256)         | NO   |     |         |                |
| om_ip         | varchar(256)         | NO   |     |         |                |
| om_port       | int(11)              | YES  |     | 0       |                |
| active_status | int(10) unsigned     | NO   |     | 0       |                |
| ucm_address   | varchar(255)         | NO   |     |         |                |
+---------------+----------------------+------+-----+---------+----------------+
19 rows in set (0.00 sec)
#查看桶数据
mysql> select * from sandstone_bucket\G
*************************** 1. row ***************************
           id: 2
         name: f082452-1
     group_id: 2
miner_node_id: 9c69b46169f5
   miner_name: f082452
  domain_host: gucloud01.sandstone-test.com
       domain: storage01
    placement: policy
     capacity: 10240
   access_key: 0fe5013ce2d014c1
   secret_key: 617e5b8a492a26c37c6458192611fcf0
        ctime: 1605852895
        mtime: 1605870472
   om_created: 1
   om_user_id: user_f082452-1
        om_ip: 192.168.0.4
      om_port: 6680
active_status: 1
  ucm_address: http://192.168.0.4:6670
1 row in set (0.00 sec)

#更新某条记录字段值
mysql> update sandstone_bucket set group_id = 3 where id=2
```



```mysql
#迁移集群涉及的一些表修改

#备份数据库
docker exec -i mysql sh -c 'exec mysqldump -uroot -proot123456 nebula_local' > nebula_local-`date +%Y%m%d%H%M%S`.sql


#修改miner的node_id
update lotus_fault_sector set miner_node_id = 'new_node' where miner_node_id = 'old_node';
update lotus_order set miner_node_id = 'new_node' where miner_node_id = 'old_node';
update lotus_sector set miner_node_id = 'new_node' where miner_node_id = 'old_node';
update lotus_window_post set miner_node_id = 'new_node' where miner_node_id = 'old_node';
update node_seal_worker_c2 set miner_node_id = 'new_node' where miner_node_id = 'old_node';
update node_seal_worker_p1 set miner_node_id = 'new_node' where miner_node_id = 'old_node';
update node_seal_worker_p1_p2_c1 set miner_node_id = 'new_node' where miner_node_id = 'old_node';
update node_seal_worker_p2 set miner_node_id = 'new_node' where miner_node_id = 'old_node';
update node_seal_worker_post set miner_node_id = 'new_node' where miner_node_id = 'old_node';
update report_day_winning_post set miner_node_id = 'new_node' where miner_node_id = 'old_node';
update sandstone_bucket set miner_node_id = 'new_node' where miner_node_id = 'old_node';


#把group_id 0修改桶的group_id 为grains的对应id
select * from sandstone_bucket;
update sandstone_bucket set group_id=2 where id =2;

```

#对于扇区非常大的表优化查询速度

```mysql
#创建表索引
create index lotus_sector_state_sector_id_end_time on lotus_sector_state (sector_id, end_time);
```



扇区状态对应编号

```mysql
//sector_state 扇区状态定义
const (
  SectorStateUndefined             = 0   // 等待中
  SectorStateEmpty                 = 1   // "Empty"
  SectorStatePacking               = 2   // "Packing"             -- sector not in sealStore, and not on chain
  SectorStatePreCommit1            = 3   // "PreCommit1"          -- do PreCommit1
  SectorStatePreCommit2            = 4   // "PreCommit2"          -- do PreCommit1
  SectorStatePreCommitting         = 5   // "PreCommitting"       -- on chain pre-commit
  SectorStatePreCommitWait         = 6   // "PreCommitWait"       -- waiting for precommit to land on chain
  SectorStateWaitSeed              = 7   // "WaitSeed"            -- waiting for seed
  SectorStateCommitting            = 8   // "Committing"
  SectorStateCommitWait            = 9   // "CommitWait"          -- waiting for message to land on chain
  SectorStateFinalizeSector        = 10  // "FinalizeSector"
  SectorStateProving               = 11  // "Proving"
  SectorStateFailedUnrecoverable   = 12  // "FailedUnrecoverable"
  SectorStateSealPreCommit1Failed  = 13  // "SealPreCommit1Failed"
  SectorStateSealPreCommit2Failed  = 14  // "SealPreCommit2Failed"
  SectorStatePreCommitFailed       = 15  // "PreCommitFailed"
  SectorStateComputeProofFailed    = 16  // "ComputeProofFailed"
  SectorStateCommitFailed          = 17  // "CommitFailed"
  SectorStatePackingFailed         = 18  // "PackingFailed"
  SectorStateFinalizeFailed        = 19  // "FinalizeFailed"
  SectorStateFaulty                = 20  // "Faulty"               -- sector is corrupted or gone for some reason
  SectorStateFaultReported         = 21  // "FaultReported"        -- sector has been declared as a fault on chain
  SectorStateFaultedFinal          = 22  // "FaultedFinal"         -- fault declared on chain
  SectorStateRemoving              = 23  // "Removing"
  SectorStateRemoveFailed          = 24  // "RemoveFailed"
  SectorStateRemoved               = 25  // "Removed"
  SectorStateWaitDeals             = 26  // "WaitDeals"
  SectorStateSubmitCommit          = 27  // "SubmitCommit"
  SectorStateDealsExpired          = 28  // "DealsExpired"
  SectorStateRecoverDealIDs        = 29  // "RecoverDealIDs"
  SectorStateAddPiece              = 30  // "SectorStateAddPiece"     -- 自定义状态AddPiece
  SectorStateP1Transmitting        = 31  // "P1Transmitting"       -- 自定义状态p1传输
  SectorStateP1TransmitFailed      = 32  // "P1TransmitFailed"     -- 自定义状态p1传输失败
  SectorStateP1                    = 33  // "P1"             -- 自定义状态p1
  SectorStateP2Transmitting        = 34  // "P2Transmitting"      -- 自定义状态p2传输
  SectorStateP2TransmitFailed      = 35  // "P2TransmitFailed"      -- 自定义状态p2传输失败
  SectorStateP2                    = 36  // "P2"             -- 自定义状态p2
  SectorStateC1                    = 37  // "C1"             -- 自定义C1
  SectorStateC2                    = 38  // "C2"             -- 自定义C2
  SectorStateFinalize              = 39  // "Finalize"           -- 自定义Finalize
  SectorStateSealedUploading       = 40  // "SealedUploading"       -- 自定义SealedUploading
  SectorStateSealedUploadingFailed = 41  // "SealedUploadingFailed"     -- 自定义SealedUploadingFailed
  SectorStateAddPieceFailed        = 42  // "AddPieceFailed"       -- 自定义AddPieceFailed
  SectorStateGetTicket             = 100 // "GetTicket"         -- GetTicket
)

```





```mysql
此文档针对集群迁移开启自动刷单出不来单问题排查处理。
#查看扇区数量
SELECT count(ls.sector_id)
FROM lotus_sector ls
WHERE ls.miner_name = 'f000000'
  AND (ls.progress in (0, 1, 3, 4, 5, 6, 7) or (ls.progress = 2 and ls.progress_status != 2) or
       (ls.progress = 8 and ls.progress_status != 1))
  and ls.remove = 0 and ls.sector_state != 25;

3、查看mq初始化是否正常
select node_id,session_id from node_storage_miner where miner_name='f000000';

#查看proving状态，程序状态为0的扇区数量，迁移数据造成的
SELECT count(ls.sector_id) FROM lotus_sector ls WHERE ls.miner_name = 'f000000'  and ls.sector_state=11 and progress=0;

#将扇区程序状态progress=24，可清理出槽位。
update lotus_sector set progress=24 WHERE miner_name = 'f000000'   and sector_state=11 and progress=0;
```





数据库慢查询设置

```
mysql> show variables like '%slow%';
+---------------------------+--------------------------------+
| Variable_name             | Value                          |
+---------------------------+--------------------------------+
| log_slow_admin_statements | OFF                            |
| log_slow_slave_statements | OFF                            |
| slow_launch_time          | 2                              |
| slow_query_log            | OFF                            |
| slow_query_log_file       | /var/lib/mysql/yangxp-slow.log |
+---------------------------+--------------------------------+
5 rows in set (0.00 sec)

mysql> set global slow_query_log=ON;
Query OK, 0 rows affected (0.00 sec)

mysql> show variables like '%slow%';
+---------------------------+--------------------------------+
| Variable_name             | Value                          |
+---------------------------+--------------------------------+
| log_slow_admin_statements | OFF                            |
| log_slow_slave_statements | OFF                            |
| slow_launch_time          | 2                              |
| slow_query_log            | ON                             |
| slow_query_log_file       | /var/lib/mysql/yangxp-slow.log |
+---------------------------+--------------------------------+
5 rows in set (0.01 sec)

mysql>
```



## **explain来了解SQL执行的状态**

explain显示了mysql如何使用索引来处理select语句以及连接表。可以帮助选择更好的索引和写出更优化的查询语句。

